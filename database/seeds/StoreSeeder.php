<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store')->insert([
            ["name" => "Doña Mary", "address" => "UTRM", "phone" => "9841234455", "email" => Str::random(10)."@mail.com"],
            ["name" => "Tienda de la esquina", "address" => "Cerrada kiwi 10", "phone" => "9823432155", "email" => Str::random(10)."@mail.com"],
        ]);
    }
}
